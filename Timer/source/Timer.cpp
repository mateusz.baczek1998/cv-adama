#include"../header/Timer.h"


void Timer::stop()
{
    begin = std::chrono::steady_clock::now();
}

void Timer::start()
{
    end = std::chrono::steady_clock::now();
}

double Timer::getTimeInMicroSec()
{
    return std::chrono::duration_cast<std::chrono::microseconds>(begin - end).count();
}