#include "../header/HelperFunctions.h"
#include  <random>
#include  <chrono>

void loadRandomDataToFile(std::string fileName)
{
	
}

void HelperClass::addSomeValues(Lista2Kier& forwardList)
{
	forwardList.pushFront(new Node{1});
	forwardList.pushFront(new Node{2});
	forwardList.pushFront(new Node{3});

	forwardList.pushFront(4);
	forwardList.pushFront(5);
	forwardList.pushFront(6);
}

void HelperClass::addSomeValues(Lista2Kier& forwardList, int howMany)
{
	size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	std::default_random_engine engine(seed);
	std::uniform_int_distribution<int>  distr(0,1000);

	int firstHalf =  howMany/2;
	int secondHalf = howMany - firstHalf;

	for (int i = 0; i<firstHalf; ++i)
	{
		forwardList.pushFront(new Node{distr(engine)});
	}
	for (int i = 0;i<secondHalf;++i)
	{
		forwardList.pushFront(distr(engine));
	}
}


void HelperClass::show(Lista2Kier& forwardList)
{
	
	Node* node = forwardList.head;
	if(node == nullptr) return;
	do
	{
		std::cout << node->key << std::endl;
		if (node->next == nullptr) break;
		node = node->next;
	} while (true);

}


// HEAP ###########################################################################################################################################
void HelperClass::addSomeValues(Heap& heap, int howMany)
{
	size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	std::default_random_engine engine(seed);
	std::uniform_int_distribution<int>  distr(0,1000);

	for (int i = 0; i<howMany; ++i)
	{
		heap.insert(distr(engine));		
	}
}


// ARRAY ###########################################################################################################################################

void HelperClass::show(Array& p_array)
{
	for(int i=0;i<p_array.size();++i)
	{
		std::cout<<p_array[i]<<std::endl;
	}
}

void HelperClass::addSomeValues(Array& array, int howMany)
{

	size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	std::default_random_engine engine(seed);
	std::uniform_int_distribution<int>  distr(0,1000);

	for(int i=0;i<howMany;++i)
	{
		array.pushFront(distr(engine));
	}
}

// Tree #######################################################################################################################3
void HelperClass::addSomeValues(RedBlackTree& tree, int howMany  = 100)
{
	size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	std::default_random_engine engine(seed);
	std::uniform_int_distribution<int>  distr(0,1000);

	for(int i=0;i<howMany;++i)
	{
		tree.addElement(distr(engine));
	}
}