//#pragma once
#include  "../../List/header/Lista2kier.h"
#include  "../../Heap/headers/Heap.h"
#include "../../Array/header/Array.h"
#include"../../RedBlackTree/headers/RedBlackTree.h"
#include <iostream>

class HelperClass
{

public:

static void addSomeValues(Lista2Kier& forwardList) ;
static void addSomeValues(Lista2Kier& forwardList, int howMany  = 100);
static void addSomeValues(Heap& heap, int howMany  = 100);
static void show(Lista2Kier& forwardList);
static void loadRandomDataToFile(std::string fileName);


static void addSomeValues(Array& array, int howMany  = 100);
static void show(Array& p_array);

static void addSomeValues(RedBlackTree& tree, int howMany  = 100);
};


