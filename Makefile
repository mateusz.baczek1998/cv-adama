CC=gcc
CXX=g++
RM=rm -fm
CXX_FLAGS=-std=c++17 -Wreturn-type -fpermissive


SRCS= main.cpp $(wildcard */*/*.cpp) $(wildcard */*/*.h) $(wildcard */*.h)
TRASH = $(wildcard */*/*.gch) $(wildcard */*.gch)

OBJS=$(subst .cc,.o,$(SRCS))

cierniak:
	$(CXX) $(CXX_FLAGS) $(SRCS)

clean:
	rm $(TRASH)
