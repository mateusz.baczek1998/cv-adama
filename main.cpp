#include <iostream>
#include "Common/header/HelperFunctions.h"
#include "Heap/headers/Heap.h"
#include "Array/header/Array.h"
#include "Timer/header/Timer.h"
#include "MenuControler/header/MenuControler.h"

using namespace std;

int main(int argc, char* argv[] ) {

// Controler
    MenuControler controler;
    controler.runControler(argc,argv);

    return 0;
}