#include "../header/Lista2kier.h"
#include<exception>
#include<iostream>

bool Lista2Kier::isEmpty()
{
	if (head == nullptr) return true;
	else return false;
}

bool Lista2Kier::pushFront(Node* node)
{
	if (head == nullptr)
	{
		node->next = nullptr;
		node->prev=nullptr;
		head =  node;
		tail = head;
		size++;
		return true;
	}
	
	node->next = head;
	node->prev = nullptr;
	head->prev = node;
	head = node;
	size++;
	return true;

}

bool Lista2Kier::pushFront(int value)
{
	return this->pushFront(new Node{value});
}

bool Lista2Kier::pushBack(Node* node) 
{
	if(tail != nullptr) 
	{
		node->next = nullptr;
		node->prev = tail;
		node->prev->next = node;
		tail = node;
		++size;
		return true;
	}
	else
	{
		node->prev = nullptr;
		node->next = nullptr;
		++size;
		tail=head=node;
		return true;
	}
}

bool Lista2Kier::pushBack(int value)
{
	return this->pushBack(new Node{value});
}

bool Lista2Kier::insert(Node* node, int position)
{
	Node* temp = head;
	if(position == 0)
	{
		//std::cout<<"Dupa1"<<std::endl;
		return pushFront(node);
	}
	else if(position == size)
	{
		//std::cout<<"Dupa2"<<std::endl;
		return pushBack(node);
	}
	else if(position <= size)
	{
		//std::cout<<"Dupa3"<<std::endl;
		for(int i = 0;i<position;++i)
		{
			//std::cout<<"Dupa4"<<std::endl;
			temp = temp->next;
		}
		//std::cout<<"Dupa5"<<std::endl;
		node->prev = temp->prev;
		node->next= temp;
		temp->prev->next = node;
		temp->prev = node;
		++size;
		return true;
	}
	else
	{
		//std::cout<<"Dupa6"<<std::endl;
		return false;
	}
}

bool Lista2Kier::insert(int value, int position)
{
	return this->insert(new Node{value}, position);
}

Node* Lista2Kier::search(int key) //tu by sie przydal std::optional
{
	Node * node = this->head;
	if(node == nullptr) return nullptr;
	while (node->key != key)
	{
		if (node == tail) return nullptr;
		node = node->next;
	}
	return node = nullptr;
}
 
bool Lista2Kier::erase(int p_value)
{
	Node* node = head;
	if(node==nullptr)return false;
	while(true)
	{
		if(node->key==p_value)
		{
			if(size ==1)
			{
				tail=head=nullptr;
				--size;
				break;
			}
			if(node->prev==nullptr)
			{
				head = node->next;
				node->next->prev = nullptr;
				break;
			}
			if(node->next == nullptr)
			{
				tail = node->prev;
				node->prev->next = nullptr;
				break;
			}
			else
			{
				node->prev->next=node->next;
				node->next->prev = node->prev;
				break;
			}
			}
	if(node->next==nullptr)
	{
		break;
	}
	node=node->next;
	
	}
	delete node;
	--size;
}