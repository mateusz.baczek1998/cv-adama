#ifndef NODE_H
#define NODE_H

struct Node
{
	int key;
	Node* next;
	Node* prev;
};

#endif