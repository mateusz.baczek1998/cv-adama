#ifndef LISTA_2_KIER_H
#define LISTA_2_KIER_H
#include"Node.h"
#include<memory>
#include"../../IStructure/IStructure.h"

class Lista2Kier : virtual public IStructure<int>
{
public:
	Node* head = nullptr;
	Node* tail = nullptr;
	size_t size=0;								// w celu testów, w releasie bedzie wyrzucone

	Lista2Kier() = default;

	bool isEmpty() override;					//sprawdza czy lista jest pusta
	bool pushFront(int value) override;			//dodaje element inicjalizowany wartoscią z argumentu
	bool pushBack(int value) override;//TODO	//dodaje element inicjalizowany wartościa z argummentu  na koniec listy
	bool insert(int value, int position) override;//dodaje element inicjalizowany wartoscią z argumentu, w dowolne miejsce na liscie (nr pozycji indeksowany od zera)
	bool erase(int p_value);					//usuwa element o podanej wartości

	
						
	Node* search(int key);				//zwraca element o szukanym kluczu
	bool pushFront(Node* node);					//dodaje element podany w argumencie  na poczatku listy
	bool insert(Node* node, int position);		//dodaje element podany w argumencie w dowolne miejsce na liscie  nr pozycji indeksowany od zera)
	bool pushBack(Node* node);					//dodaje element podany w argumencie na koniec listy
};



#endif
