#ifndef RED_BLACK_TREE
#define RED_BLACK_TREE

#define COLOR bool
#define RED true
#define BLACK false

#include <fstream>

//#include "Structure.h"
//Deklaracja klasy: RedBlackTree, dziedziczącej po klasie: Structure, reprezentującej strukturę drzewa czerwono-czarnego
class RedBlackTree// : public Structure
{
public:

	int numberOfElements = 0;
	//Deklaracja klasy wewnętrznej: Node, której obiekty są węzłami w drzewie czerwono-czarnych
	class Node {
	public:
		Node *right;		//Deklaracja wskaźnika na prawe dziecko węzła
		Node *left;			//Deklaracja wskaźnika na lewe dziecko węzła
		Node *parent;		//Deklaracja wskaźnika na rodzica węzła
		int key;			//Deklaracja klucza węzła
		COLOR color;		//Deklaracja koloru węzła

		//Definicja kontruktora, przyjmującego w argumencie wartość klucza
		Node(int key) {
			right = nullptr;		///Przypisanie NULL do wskaźnika na prawe dziecko
			left = nullptr;		///Przypisanie NULL do wskaźnika na lewe dziecko
			parent = nullptr;		///Przypisanie NULL do wskaźnika na rodzica
			Node::key = key;	///Przypisanie wartości: key do kluczowi węzła
			color = NULL;		///Każdy dodany węzeł, którego klucz nie jest równy NULL, jest czerwony		
		}
		//Definicja konstruktora domyślnego
		Node() {
			right = nullptr;		///Przypisanie NULL do wskaźnika na prawe dziecko
			left = nullptr;		///Przypisanie NULL do wskaźnika na lewe dziecko
			parent = nullptr;		///Przypisanie NULL do wskaźnika na rodzica
			color = BLACK;		///Przypisanie koloru: BLACK nowemu węzłowi
		}
	};
	Node *mainNode;											//Deklaracja wskaźnika na pierwszy węzeł (korzeń) drzewa czerwono-czarnego
	Node sentinelNode;										//Deklaracja węzła-znacznika, który reprezentuje węzeł pusty
	RedBlackTree();									//Deklaracja kontruktora, którego argument jest wskaźnikiem na obiekt klasy: Timer
	~RedBlackTree();										//Deklaracja destruktora domyślnego 
	void menuAddElement();									//Deklaracja funkcji, zarządzającej dodawaniem elementu do struktury
	void freeMem();											//Deklaracja funkcji, zwalniającej pamięć
	void freeMem(Node * node);								//Deklaracja funkcji rekurencyjnej, zwalniającej pamięć
	void menuDeleteElement();								//Deklaracja funkcji, zarządzającej usuwaniem elementu ze struktury
	double loadDataFromFile(std::fstream *file);			//Deklaracja funkcji, wczytującej elementy struktury z pliku
	Node * BSTsearch(Node *node,int key);

public:
	
	void addElement(int key);								//Deklaracja funkcji, dodającej do struktury element z kluczem: key
	void leftRotation(Node * node);							//Deklaracja funkcji, wykonującej rotację w lewo wzgledem węzła: node
	void rightRotation(Node * node);						//Deklaracja funkcji, wykonującej rotację w prawo względem węzła: node
	void deleteElement(int key);							//Deklaracja funkcji, wyszukującej, a następnie usuwającej węzęł z kluczem: key
	void deleteFIX(Node * doubleBlackNode);
	RedBlackTree::Node * findSuccessor(Node * node);		//Deklaracja funkcji, zwracającej następnika węzła: node
	bool search(int key);									//Deklaracja funkcji, wyszukującej węzeł z kluczem: key
	void drawTree(Node * node, int indent);					//Deklaracja funkcji rekurencyjnej, rysującej drzewo
	void insertFIX(Node * newNode);
};

#endif