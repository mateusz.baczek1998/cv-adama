#include "../headers/RedBlackTree.h"
#include <iostream>

//Function: RedBlackTree::RedBlackTree(Timer *timer)
//Type: kontruktor
//Parameters::
//	1) Timer *timer - wskaźnik na obiekt klasy: Timer
//Return value: *RedBlackTree
//Description:
//Konstruktor przyjmuje za argument wskaźnik na obiekt klasy: Timer, który służyć będzie do mierzenia czasu wykonania różnych operacji na strukturach danych. Następuje również inicjalizacja węzła-znacznika.
RedBlackTree::RedBlackTree() {
	sentinelNode.color = BLACK;
	sentinelNode.left = &sentinelNode;
	sentinelNode.right = &sentinelNode;
	sentinelNode.parent = &sentinelNode;
	mainNode = &sentinelNode;
}

//Function: RedBlackTree::~RedBlackTree()
//Type: destruktor
//Parameters: none
//Return value: void
//Description:
//Destruktor wywołuje funkcję Structure::freeMem(), która zwalniania pamięć zajętą przez drzewo, czyli wszystkie węzły drzewa oraz główny węzeł (korzeń drzewa). 
RedBlackTree::~RedBlackTree() {
//	freeMem();
//	delete mainNode;
}


//Function: RedBlackTree::addElement(int key) 
//Parameters::
//	1)	int key - klucz nowego elementu
//Return value: double - czas wykonania funkcji
//Description:
//Funkcja dodaje do drzewa nowy węzeł z kluczem o wartości: key. Algorytm jest wzorowany na tym, przedstawionym w książce "Wprowadzenie do algorytmiki" Cormen'a na stronie 310 polskiego wydania. 
//Proces wykonywany jest następująco: najpierw dynamicznie utworzony nowy węzeł dodawany jest za pomocą standardowego algorytmu dodawania elementu do drzewa BST, a następnie przywracany jest porządek drzewa czerwono-czarnego.
void RedBlackTree::addElement(int key) {
	Node *tempNode = mainNode;
	Node *newNode = new Node(key);
	newNode->left = &sentinelNode;
	newNode->parent = &sentinelNode;
	newNode->right = &sentinelNode;
	newNode->color = RED;
	//Dodanie elementu za pomocą zwykłego dodawania do drzew BST
	//Ustalenie rodzica
	if (mainNode != &sentinelNode) {
		Node **freeNode;
		while (true) {
			if (key < tempNode->key)
				freeNode = &(tempNode->left);
			else
				freeNode = &(tempNode->right);

			if (*freeNode == &sentinelNode) {
				newNode->parent = tempNode;
				*freeNode = newNode;
				break;
			}
			tempNode = *freeNode;
		}

		insertFIX(newNode);
	}
	else
		mainNode = newNode;
	mainNode->color = BLACK;
}

void RedBlackTree::insertFIX(Node *newNode) {
	if (newNode->parent->color == RED) {
		Node* parent = newNode->parent;
		Node* grandParent = parent->parent;
		Node* uncle;
		if (grandParent->left == parent)
			uncle = grandParent->right;
		else
			uncle = grandParent->left;
		//Przypadek 1.: Uncle jest czerwony
		if (uncle != &sentinelNode &&uncle->color == RED) {
			parent->color = BLACK;
			uncle->color = BLACK;
			if (grandParent != mainNode) {
				grandParent->color = RED;
				insertFIX(grandParent);
			}
		}
		else {
			//Przypadek 2.: Uncle jest czarny (lustrzane przypadki)
			if (newNode == parent->left &&
				parent == grandParent->right || parent == grandParent->left && newNode == parent->right) {
				if (newNode == parent->right)
					leftRotation(parent);
				else
					rightRotation(parent);
				Node* temp = parent;
				parent = newNode;
				newNode = temp;
			}
			if (newNode == parent->left)
				rightRotation(grandParent);
			else
				leftRotation(grandParent);

			parent->color = BLACK;
			grandParent->color = RED;
		}
	}
}

//Function: RedBlackTree::leftRotation(Node *node) 
//Parameters::
//	1)	Node *node - wskaźnik na węzeł, wzgledem którego wykonywana jest rotacja w lewo
//Return value: void
//Description:
//Zadaniem funkcji jest wykonanie rotacji węzłów w lewo. Algorytm wzorowany jest na tym, przedstawionym w książce "Wprowadzenie do algorytmiki" Corman'a na stronie 308 polskiego wydania.
void RedBlackTree::leftRotation(Node *node) {
	Node *tempNode = node->right;
	tempNode->parent = node->parent;
	if (node->parent == &sentinelNode)
		mainNode = tempNode;
	else
		if (node == tempNode->parent->left)
			tempNode->parent->left = tempNode;
		else
			tempNode->parent->right = tempNode;

	node->right = tempNode->left;
	if (tempNode->left != &sentinelNode)
		tempNode->left->parent = node;
	node->parent = tempNode;
	tempNode->left = node;

}

//Function: RedBlackTree::rightRotation(RedBlackTree::Node *node) 
//Parameters::
//	1)	Node *node - wskaźnik na węzeł, względem którego wykonywana jest rotacja w prawo
//Return value: void
//Description: 
//Zadaniem funkcji jest wykonanie rotacje węzłów w prawo. Algorytm wzorowany jest na tym, przedstawionym w książce "Wprowadzenie do algorytmiki" Corman'a na stronie 308 polskiego wydania dla rotacji lewej. Rotacja w prawo jest symetryczna do lewej.
void RedBlackTree::rightRotation(Node *node) {
	Node *tempNode = node->left;

	tempNode->parent = node->parent;
	if (node->parent == &sentinelNode)
		mainNode = tempNode;
	else
		if (node == tempNode->parent->left)
			tempNode->parent->left = tempNode;
		else
			tempNode->parent->right = tempNode;

	node->left = tempNode->right;
	if (tempNode->right != &sentinelNode)
		tempNode->right->parent = node;
	node->parent = tempNode;
	tempNode->right = node;

}

//Function: Structure::search(int key) 
//Parameters::
//	1)	int key - wartość szukanego w drzewie klucza
//Return value: double - czas wykonania funkcji
//Description:
//Funkcja wyszukuje węzeł o zadanym kluczu: key. Pętla szukająca kończy się po znalezieniu pierwszego węzła z kluczem równym kluczowi szukanemu. Jeśli taki węzeł nie zostanie znaleniony zostanie zwrócone 0.
bool RedBlackTree::search(int key) {
	Node *node = BSTsearch(mainNode, key);
	if (node != &sentinelNode)
		return true;
	
	return false;
}



//Function: RedBlackTree::BSTsearch(RedBlackTree::Node *node, int key) 
//Parameters::
//	1)	RedBlackTree::Node *node - potencjalny węzeł, zawierający klucz o wartości: key
//	2)	int key - szukana wartość klucza
//Return value: RedBlackTree::Node* - wskaźnik na węzeł, posiadający odpowiedni klucz
//Description:
//Funkcja wyszukująca rekurencyjnie węzeł o kluczu: key. Algorytm jest typowym algorytmem wyszukiwania w drzewach BST.
RedBlackTree::Node* RedBlackTree::BSTsearch(Node* node, int key) {
	if (node == &sentinelNode || node->key == key)
		return node;
	if (node->key > key)
		return BSTsearch(node->left, key);
	else
		return BSTsearch(node->right, key);
}

//Function: RedBlackTree::deleteElement(int key) 
//Parameters::
//	1)	int key - klucz węzła do usunięca
//Return value: double - czas wykonania funkcji
//Description:
//Funkcja znajduje węzeł z kluczem: key, a następnie usuwa go z drzewa. Kolejne etapy etapy procesu usuwania węzła to: znalezienie odpowiedniego węzła za pomocą funkcji RedBlackTree::BSTsearch(Node *node, int key), 
//usunięcie tego węzła, a następnie przywrócenie właściwości drzewa czerwono-czarnego.
void RedBlackTree::deleteElement(int key) {
	Node *toDelNode = &sentinelNode;
	toDelNode = BSTsearch(mainNode, key);
	if (toDelNode != &sentinelNode) {
		//Przypadek zero: brak synów
		if (toDelNode->right == &sentinelNode && toDelNode->left == &sentinelNode) {
			if (toDelNode->parent != &sentinelNode) {
				if (toDelNode->parent->left == toDelNode)
					toDelNode->parent->left = &sentinelNode;
				else
					toDelNode->parent->right = &sentinelNode;
			}
		}
		//Przypadek pierwszy: tylko prawy syn
		if (toDelNode->right != &sentinelNode && toDelNode->left == &sentinelNode) {
			toDelNode->right->parent = toDelNode->parent;
			if (toDelNode->parent->left == toDelNode)
				toDelNode->parent->left = toDelNode->right;
			else
				toDelNode->parent->right = toDelNode->right;

			if (toDelNode->color == BLACK && toDelNode->right->color == RED && toDelNode->parent->color == RED) {
				toDelNode->right->color = BLACK;
			}
		}
		//Przypadek drugi: tylko lewy syn
		if (toDelNode->left != &sentinelNode && toDelNode->right == &sentinelNode) {
			toDelNode->left->parent = toDelNode->parent;
			if (toDelNode->parent->left == toDelNode)
				toDelNode->parent->left = toDelNode->left;
			else
				toDelNode->parent->right = toDelNode->left;
			if (toDelNode->color == BLACK && toDelNode->left->color == RED && toDelNode->parent->color == RED) {
				toDelNode->left->color = BLACK;
			}
		}
		else {
			//Przypadek trzeci: dwóch synów, prawy jest następnikiem
			Node *successor = findSuccessor(toDelNode);
			Node *doubleBlackNode = &sentinelNode;
			if (successor == toDelNode->right) {
				toDelNode->key = successor->key;
				toDelNode->right = successor->right;
				if (successor->right != &sentinelNode)
					successor->right->parent = toDelNode;
				if (successor->color == BLACK)
					doubleBlackNode = successor->right;
				*successor = sentinelNode;
			}
			//Przypadek czwarty: następnik nie jest prawym synem toDelNode
			else
			{
				if (successor->color == BLACK && successor->right->color == BLACK)
					doubleBlackNode = successor->right;
				toDelNode->key = successor->key;
				successor->parent->left = successor->right;
				successor->right->parent = successor->parent;
			}
			deleteFIX(doubleBlackNode);
		}
	}
	
}

void RedBlackTree::deleteFIX(Node *doubleBlackNode) {
	//NAPRAWA warunków drzewa
	if (doubleBlackNode->parent->parent->left == doubleBlackNode->parent) {
		if (doubleBlackNode != &sentinelNode) {
			//Przypadek 1: brat doubleBlackNode jest czerwony
			if (doubleBlackNode->parent->right->color == RED) {
				leftRotation(doubleBlackNode->parent);
				COLOR temp = doubleBlackNode->parent->color;
				doubleBlackNode->parent->color = doubleBlackNode->parent->parent->color;
				doubleBlackNode->parent->parent->color = temp;
			}
			//Przypadek 2: brak doubleBlackNode jest czarny i ma dwóch synów
			if (doubleBlackNode->parent->right->color == BLACK && doubleBlackNode->parent->right->left != &sentinelNode && doubleBlackNode->parent->right->right != &sentinelNode) {
				doubleBlackNode = doubleBlackNode->parent;
				doubleBlackNode->right->color = RED;
				deleteFIX(doubleBlackNode);
			}
			//Przypadek 3: Brat doublBlackNode jest czarny, lewy syn brata jest czerwony, a prawy czarny
			if (doubleBlackNode->parent->right->color == BLACK && doubleBlackNode->parent->right->right->color == BLACK && doubleBlackNode->parent->right->left->color == RED) {
				rightRotation(doubleBlackNode->parent->right);
				COLOR temp = doubleBlackNode->parent->right->color;
				doubleBlackNode->parent->right->color = doubleBlackNode->parent->right->right->color;
				doubleBlackNode->parent->right->right->color = temp;
			}
			//Przypadek 4: Brat doubleBlackNode jest czarny, prawy syn jest czerwony
			if (doubleBlackNode->parent->right->color == BLACK && doubleBlackNode->parent->right->right->color == RED) {
				leftRotation(doubleBlackNode->parent);
				doubleBlackNode->parent->parent->color = doubleBlackNode->parent->color;
				doubleBlackNode->parent->color = BLACK;
				doubleBlackNode->parent->parent->right->color = BLACK;
			}
		}
	}
	else {
		if (doubleBlackNode != &sentinelNode) {
			//Przypadek 1: brat doubleBlackNode jest czerwony
			if (doubleBlackNode->parent->left->color == RED) {
				rightRotation(doubleBlackNode->parent);
				COLOR temp = doubleBlackNode->parent->color;
				doubleBlackNode->parent->color = doubleBlackNode->parent->parent->color;
				doubleBlackNode->parent->parent->color = temp;
			}
			//Przypadek 2: brak doubleBlackNode jest czarny i ma dwóch synów
			if (doubleBlackNode->parent->left->color == BLACK && doubleBlackNode->parent->left->right != &sentinelNode && doubleBlackNode->parent->left->left != &sentinelNode) {
				doubleBlackNode = doubleBlackNode->parent;
				doubleBlackNode->left->color = RED;
				deleteFIX(doubleBlackNode);
			}
			//Przypadek 3: Brat doublBlackNode jest czarny, lewy syn brata jest czerwony, a prawy czarny
			if (doubleBlackNode->parent->left->color == BLACK && doubleBlackNode->parent->left->left->color == BLACK && doubleBlackNode->parent->left->left->color == RED) {
				leftRotation(doubleBlackNode->parent->left);
				COLOR temp = doubleBlackNode->parent->left->color;
				doubleBlackNode->parent->left->color = doubleBlackNode->parent->left->left->color;
				doubleBlackNode->parent->left->left->color = temp;
			}
			//Przypadek 4: Brat doubleBlackNode jest czarny, prawy syn jest czerwony
			if (doubleBlackNode->parent->left->color == BLACK && doubleBlackNode->parent->left->left->color == RED) {
				leftRotation(doubleBlackNode->parent);
				doubleBlackNode->parent->parent->color = doubleBlackNode->parent->color;
				doubleBlackNode->parent->color = BLACK;
				doubleBlackNode->parent->parent->left->color = BLACK;
			}
		}
	}
}

//Function: RedBlackTree::findSuccessor(RedBlackTree::Node *node)
//Parameters::
//	1)	RedBlackTree::Node *node - wskaźnik na węzeł, którego następnika funkcja ma znaleźć
//Return value: RedBlackTree::Node* - wskaźnik na następnika węzła: node
//Description:
//Funkcja ma za zadanie znaleźć i zwrócić wskaźnik na następnika węzła: node. Algorytm inspirowany tym, znajdującym się w książce pt.: "Wprowadzenie do algorytmiki" Cormane'a na stronie 289 polskiego wydania.
RedBlackTree::Node* RedBlackTree::findSuccessor(Node *node) {
	Node *tempNode = node;

	if (node->right != &sentinelNode) {
		tempNode = node->right;
		while (tempNode->left != &sentinelNode)
			tempNode = tempNode->left;
		return tempNode;
	}
	else
	{
		Node *parent = tempNode->parent;
		while (parent != &sentinelNode && (parent == tempNode->right)) {
			parent = tempNode;
			tempNode = tempNode->parent;
		}
		return tempNode;
	}
	return node;
}


//Function: Structure::freeMem() 
//Parameters: none
//Return value: void
//Description:
//Funkcja wywołuje wszystkie niezbędne do zwolnienia pamięci zajmowanej przez drzewo czerowno-czarne funkcje.
void RedBlackTree::freeMem() {
	freeMem(mainNode);
	mainNode = &sentinelNode;
	numberOfElements = 0;
}

//Function: RedBlackTree::freeMem(RedBlackTree::Node *node) 
//Parameters::
//	1)	RedBlackTree::Node *node - wskaźnik na węzeł, którego podrzewa będą następnie usuwane
//Return value: void
//Description:
//Funkcja zwalnia miejsce zajmowane przez kolejne węzły wykonując podróż po drzewie metodą inorder
void RedBlackTree::freeMem(Node *node) {
	if (mainNode == &sentinelNode)
		return;
	else if (node == &sentinelNode)
		node = mainNode;

	if (node->left != &sentinelNode)
		freeMem(node->left);
	if (node->right != &sentinelNode)
		freeMem(node->right);
	delete (node);
}
