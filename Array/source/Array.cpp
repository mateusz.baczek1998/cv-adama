#include  "../header/Array.h"
#include<iostream>//wywalic
Array::Array()
{
    m_CurrentSize = 0;
}
bool Array::isEmpty()
{
        if(m_CurrentSize == 0 )
        {
            return true;
        }
        else return false;
}	

unsigned int Array::size()
{
    return m_CurrentSize;
}

int& Array::operator[](int n)
{
    return m_array[n];
}
std::unique_ptr<int[]> Array::realloc()
{
    return std::make_unique<int[]>(m_CurrentSize+1);
}
bool Array::pushFront(int value)
{
    auto tmp = this->realloc();
    if(tmp!=nullptr)
    {
        ++m_CurrentSize;
        tmp[0]=value;
        for(int i=1;i<m_CurrentSize;++i)
        {
            tmp[i]=m_array[i-1];
        }
        m_array.swap(tmp);
        tmp.release();
        return true;
    }
    else return false;
}	


bool Array::pushBack(int value)
{   

    auto tmp = this->realloc();

    if(tmp != nullptr)
    {
        for(int i=0; i< m_CurrentSize;++i)
        {
            tmp[i] = m_array[i];
        }
        ++m_CurrentSize;
        tmp[m_CurrentSize-1] = value;
        m_array.swap(tmp);
        tmp.release();
        return true;
    }
    else return false;
}

bool Array::insert(int value,int position)
{
    auto tmp = this->realloc();

    if(position==0)
    {
        this->pushFront(value);
    }
    else if(position == m_CurrentSize)
    {
        this->pushBack(value);
    }
    else if(position<m_CurrentSize && position>0)
    {
        tmp[position] = value;
        ++m_CurrentSize;

        for(int i=0,j=0;j<m_CurrentSize;++j)
        {
            if(j!=position)
            {
                tmp[j]=m_array[i];
                i++;
            }
        }

        m_array.swap(tmp);
        tmp.release();
        return true;
    }
    else return false;
}
int Array::search(int key)
{
    for(int i=0;i<m_CurrentSize;++i)
    {
        if(m_array[i]==key)
        return i;
    }
    return -1;
}
bool Array::erase(int index)
{

    if(index >-1 && index<m_CurrentSize)
    {
        auto tmp = std::make_unique<int[]>(m_CurrentSize-1);
        for(int j=0,i=0;i<m_CurrentSize;++i)
        {
            if(i != index)
            {
                tmp[j] = m_array[i];
                ++j;
            }
        }

        --m_CurrentSize;
        m_array.swap(tmp);
        tmp.release();
        return true;
    }
    else
    {
        return false;
    }
   
}

