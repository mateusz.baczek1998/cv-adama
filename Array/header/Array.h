#pragma  once

#include"../../IStructure/IStructure.h"
#include<memory>

/* 
	- talica tworzona jest z ustaloną  ilością elementów 
	i w razie potrzeby jest powiększana
*/

class  Array: public IStructure<int>
{
private:

	std::unique_ptr<int[]> m_array;
	unsigned int m_CurrentSize;
	//unsigned int m_MaxSize;

public:
    bool isEmpty() override;
	bool pushFront(int value) override;
	bool pushBack(int value) override;
	bool insert(int value,int position) override;
	bool erase(int value) override;
	
	Array();
	int  search(int key);	//zwraca indeks szukanego  elementu
	unsigned int size();
	int& operator[](int n);
	std::unique_ptr<int[]> realloc();
};