//#define _GLIBCXX_USE_CXX11_ABI 0
#include "../headers/Heap.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

Heap::Heap(int size) {
    this -> size = size;
    heapData = new int [size];

    heapPointer = -1;
}

void Heap::insert(int value) {
    if(this->heapPointer + 1 == this->size)
        return; //TODO: errors? boolean return type?

    heapPointer++;

    heapData[heapPointer] = value;
    maxHeapify(Heap::parent(heapPointer));

    //print();
    
}

void Heap::maxHeapify(int parent) {
    //print();
    int left = (parent + 1) * 2 - 1;
    int right = (parent + 1) * 2;

    //cout << " comparing " << parent << ": "<< right << ", " << left << endl;

    int largest = parent;

    if(left <= heapPointer && heapData[left] > heapData[largest])
    {
        largest = left; 
    }

    if(right <= heapPointer && heapData[right] > heapData[largest])
    {
        largest = right;
    }

    if(largest != parent) {
        //std::cout << "swapping " << heapData[largest] << " with " << heapData[parent] << std::endl; 
        std::swap(heapData[largest], heapData[parent]);
        //int temp = heapData[largest];
        //heapData[largest] = heapData[parent];
        //heapData[parent] = temp;
        //print();


        if(parent != 0)
            maxHeapify(Heap::parent(parent));
    }
}

void Heap::print() {

    const int width = 62;

    int treeLevel = 1;
    int nodeNumberOnLevel = 0;
    int howManyNodesOnCurrentLevel = 1;

    for(int i = 0; i <= this->heapPointer; i ++) {
        

        int spaceBetweenNodes = width / howManyNodesOnCurrentLevel;

        if (nodeNumberOnLevel != 0)
            for (int j = 0; j < spaceBetweenNodes; j++) {
                std::cout << " ";
            }
        else {
            for (int j = 0; j < spaceBetweenNodes/2; j++) {
                std::cout << " ";
            }
        }

        std::cout << heapData[i];


        nodeNumberOnLevel++;

        if(nodeNumberOnLevel >= howManyNodesOnCurrentLevel) {
            nodeNumberOnLevel = 0;
            howManyNodesOnCurrentLevel *= 2;
            treeLevel ++;

            std::cout << std::endl;
        }
    }

   
    std::cout << std::endl;
    std::cout << "=================================" << std::endl;
    std::cout << std::endl;
}



/*Funkcja usuwajaca element z korzenia.*/
void Heap::pop() {
	//jeśli kopiec pusty to koniec
	if (heapPointer == 0)
		return;
	//wstawienie ostatniego liścia na miejsce korzenia
	
	std::swap (heapData[0], heapData[heapPointer]);
	//usunięcie ostatniego liścia
	heapPointer--;
	
    fixDownwards(0);
	
}




/*Funkcja naprawiajaca kopiec w dol*/
void Heap::fixDownwards(int nodeIndex)
{ 
    //print();
    int left = (nodeIndex + 1) * 2 - 1;
    int right = (nodeIndex + 1) * 2;

    if(left <= heapPointer && heapData[left] > heapData[nodeIndex])
    {
        //cout << "swapping " << heapData[left] << " and "<< heapData[nodeIndex] << endl;
        
        std::swap(
            heapData[nodeIndex],
            heapData[left]
        );
        //print();
        fixDownwards(left);
    }

    else if(right <= heapPointer && heapData[right] > heapData[nodeIndex])
    {
        //cout << "swapping " << heapData[right] << " and "<< heapData[nodeIndex] << endl;
        std::swap(
            heapData[nodeIndex],
            heapData[right]
        );
        //print();
        fixDownwards(right);
    }
}


int Heap::indexOf(int value) {
    return indexOf(value, 0);
}


int Heap::indexOf(int value, int root) {

    //cout << "searching in " << root << endl;
    if(root > heapPointer || heapData[root] < value) {

        //cout << "Wont be in children" << endl;
        return -1;
    }

    int left = (root + 1) * 2 - 1;
    int right = (root + 1) * 2;

    if(left <= heapPointer && heapData[left] == value) {
        return left;
    }

    if(right <= heapPointer && heapData[right] == value) {
        return right;
    }

    int searchLeft = indexOf(value, left); 
    
    if(searchLeft != -1) {
        return searchLeft;
    }

    return indexOf(value, right);


}


void Heap::loadFromFile(std::string filename) {
    fstream in = fstream(filename, ios::in);

    std::string line;
    int number;

    while(getline(in, line)) {
        
        int value = std::stoi(line);
        this->insert(value);

        
    }
}