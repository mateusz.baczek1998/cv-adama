//#define _GLIBCXX_USE_CXX11_ABI 0
#ifndef HEAP_H
#define HEAP_H
#include <math.h>
#include <string>

using namespace std;

class Heap {

        int size;
        int * heapData;
        int heapPointer;

    public:
        Heap(int size);
        void insert(int value);
        void erase(int value);
        void maxHeapify(int parent);

        static int parent(int element) {
            return (element - 1) / 2;
        }

        void pop();
        void fixDownwards(int nodeIndex);

        int indexOf(int value);
        int indexOf(int value, int root);

        void print();

        void loadFromFile(std::string filename);

};

#endif