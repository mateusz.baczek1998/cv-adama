# Zadanie projektowe 1

## Badanie efektywności operacji dodawania, usuwania oraz wyszukiwania elementów w różnych strukturach danych.

### Mateusz Bączek, 241330






## Zarys problemu


Celem projektu była implementacja i analiza zachowań podstawowych struktur danych.
Testowane były operacje tworzenia struktury, dodawania i usuwania elementów
oraz wyszukiwanie klucza.

Testowane struktury:

* Tablica
* Lista dwukierunkowa
* Kopiec
* Drzewo czerwono-czarne



## Sposób testowania


Dla każdej struktury zmierzony został czas wykonywania operacji:

* Budowania struktury o rozmiarze N
* Dodawania elementu do struktury o rozmiarze N
* Usuwanie elementu ze struktury o rozmiarze N
* Wyszukiwanie klucza w strukturze o rozmiarze N

Gdzie N przyjmuje kolejne wartości:

* od 1 do 2000 w przypadku tablicy i listy
* od 1 do 4000000 w przypadku kopca i drzewa czerwono-czarnego

Następnie otrzymane czasy nanoszone są na wykres.

Program posiada też menu tekstowe, które pozwala w dowolnej kolejności wykonywać dowolne operacje
na wybranej przez użytkownika strukturze, pozwala wyświetlić zawartość struktury i pokazuje czas wykonania każdej operacji w mikrosekundach.



## Tablica


Tablica – kontener uporządkowanych danych takiego samego typu, w którym poszczególne
elementy dostępne są za pomocą kluczy (indeksu). Indeks najczęściej przyjmuje wartości
numeryczne. Rozmiar tablicy jest albo ustalony z góry (tablice statyczne), albo może się
zmieniać w trakcie wykonywania programu (tablice dynamiczne). Tablice jednowymiarowe

mogą przechowywać inne tablice, dzięki czemu uzyskuje się tablice wielowymiarowe. W tablicach
wielowymiarowych poszczególne elementy są adresowane przez ciąg indeksów.

Praktycznie wszystkie języki programowania obsługują tablice – jedynie w niektórych
językach funkcyjnych zamiast tablic używane są listy (choć tablice zwykle też są dostępne).
W matematyce odpowiednikiem tablicy jednowymiarowej jest ciąg, a tablicy dwuwymiarowej - macierz.




```
tablica.print()

[101 118 97 110 103 101 108 105 111 110]
```


![](out21.png)

![](out22.png)

![](out23.png)

![](out24.png)

## Lista


Lista – struktura danych służąca do reprezentacji zbiorów dynamicznych, w której elementy

ułożone są w liniowym porządku. Rozróżniane są dwa podstawowe rodzaje list: lista jednokierunkowa,
w której z każdego elementu możliwe jest przejście do jego następnika oraz lista

dwukierunkowa w której z każdego elementu możliwe jest przejście do jego poprzednika i
następnika



```
lista.print()

[114 105 99 97 114 100 111]
```


![](out11.png)

![](out12.png)

![](out13.png)

![](out14.png)

## Kopiec


Kopiec (ang. heap, tłumaczone też jako stóg lub sterta) – struktura danych oparta na drzewie,
w której wartości potomków węzła są w stałej relacji z wartością rodzica (na przykład wartość
rodzica jest nie mniejsza niż wartości jego potomka).
Kopce mogą być używane do implementacji kolejek priorytetowych, ponieważ ustalenie 
odpowiedniej relacji umożliwia bezpośredni dostęp do wierzchołka o największej lub naj
mniejszej wartości z danego zbioru zapisanego w kopcu.


Drugie częste zastosowanie struktury kopca to sortowanie. Wstawianie nowych wierzchołków
do kopca porządkuje je. Następnie możemy zdejmować obiekty ze szczytu kopca, tak długo aż są w kopcu jakieś obiekty.
Otrzymamy wówczas ciąg obiektów uporządkowany według klucza. Na tej zasadzie działa procedura sortowania przez kopcowanie (ang. heapsort).



```
kopiec.print()

                               96
               95                               92
       86               88               61               86
   34       78       83       50       29       54       11       52
```


![](out31.png)

![](out33.png)

![](out32.png)

![](out34.png)

## Drzewo czerwono-czarne


Drzewo czerwono-czarne – rodzaj samoorganizującego się binarnego drzewa poszukiwań -

struktury danych stosowanej w informatyce najczęściej do implementacji tablic asocjacyjnych.
Została ona wynaleziona przez Rudolfa Bayera w 1972 roku, który nazwał je symetrycznymi
binarnymi B-drzewami. Współczesną nazwę oraz dokładne zbadanie ich właściwości
zawdzięcza się pracy *A dichromatic framework for balanced trees* 1978 roku autorstwa 
Leo J. Guibasa oraz Roberta Sedgewicka.
Drzewa czerwono-czarne są skomplikowane w implementacji, lecz charakteryzują się **niską**
złożonością obliczeniową elementarnych operacji takich, jak wstawianie, wyszukiwanie czy
usuwanie elementów z drzewa.


![](out41.png)

![](out42.png)

![](out43.png)

![](out44.png)

## Wnioski


Po samodzielnym zaimplementowaniu i przetestowaniu powyższych struktur danych,
najważniejsze wnioski wyraziłem w tabeli:


| Struktura | Trudność implementacji | Kiedy najlepiej użyć | Kiedy **nie używać** |
| ------ | ------ | --- | --- |
| Tablica | Dziecinnie łatwa | Dane niezmiennej długości, mało dostępnych zasobów | Częste zmiany długości tablicy |
| Lista | Łatwa | Dużo operacji dopisywania/kasowania elementów | Brak specjalnych przeciwwskazań |
| Kopiec | Średnia | Potrzeba wyszukania największej/najmniejszej wartości w zbiorze | Brak potrzeby sortowania/przechowywania w danym porządku |
| Drzewo czerwono-czarne | **Trudna** | Szybkie wyszukiwanie dowolnego klucza | Brak potrzeby szybkiego wyszukiwania, potrzeba prostej implementacji |





