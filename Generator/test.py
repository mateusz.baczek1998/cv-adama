import subprocess
from tqdm import tqdm # Progress bar library
import pypandoc
import subprocess
import matplotlib.pyplot as plt

USE_CACHED = False
ITERATIONS = 2000
STYLE = 'style3.css'

class MarkdownDocument:
    def __init__(self, filename, style_css='style2.css'):
        self.filename = filename
        self.file = open(filename, 'w')
        self.style_css = style_css


    def add_header(self, text, size=2):
        self.file.write(size * '#' + f' {text}\n\n')

    def add_image(self, image, alt_text=''):

        self.file.write(
            f'![{alt_text}]({image})\n\n'
        )

    def add_text(self, text):
        self.file.write( f'{text}\n\n')

    def save(self):
        print('# Saving..')
        self.file.close()

        print('# Generating PDF..')
        output = pypandoc.convert_file(
            self.filename,
            'html',
            format='md',
        )

        html_output = open(self.filename.replace('.md', '') + '.html', 'w')
        html_output.write(output)
        html_output.close()

        pdf_command = f'pandoc -t html5 --css {self.style_css} {self.filename} -o ' + self.filename.replace('.md', '') + '.pdf'
        print(f'# Calling `{pdf_command}`')
        stream = subprocess.Popen(
            #[executable_to_call, str(i)],
            pdf_command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True)
        output = stream.communicate(timeout=100)


def generate_plot(executable_to_call, argument_from, argument_to, outfile, *label):

    if USE_CACHED:
        return

    plt.cla() # Clears the figure
    plt.clf()

    print(f'# Generating plot for {executable_to_call}, {argument_from}-{argument_to}...')

    x = []
    y = []
    
    for i in tqdm(range(argument_from, argument_to)):
    #for i in range(argument_from, argument_to):
        stream = subprocess.Popen(
            #[executable_to_call, str(i)],
            f'{executable_to_call} {i}',
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True)
        output = stream.communicate(timeout=100)

        #print(output[0].decode('utf-8'))
        y.append(int(output[0].decode('utf-8')))
        x.append(i)

    plt.scatter(x,y, s=8)
    
    plt.legend(label, loc='upper left')
    plt.xlabel('Ilość elementów w strukturze')
    plt.ylabel('Czas wykonania [µs]')

    plt.savefig(outfile, dpi=220)


document = MarkdownDocument('test.md', STYLE)


document.add_header('Zadanie projektowe 1', size=1)
document.add_header('Badanie efektywności operacji dodawania, usuwania oraz wyszukiwania elementów w różnych strukturach danych.')
document.add_header('Mateusz Bączek, 241330', size=3)

document.add_text('\n\n\n')

document.add_header('Zarys problemu')
document.add_text(
'''
Celem projektu była implementacja i analiza zachowań podstawowych struktur danych.
Testowane były operacje tworzenia struktury, dodawania i usuwania elementów
oraz wyszukiwanie klucza.

Testowane struktury:

* Tablica
* Lista dwukierunkowa
* Kopiec
* Drzewo czerwono-czarne

'''
)


document.add_header('Sposób testowania')
document.add_text(
'''
Dla każdej struktury zmierzony został czas wykonywania operacji:

* Budowania struktury o rozmiarze N
* Dodawania elementu do struktury o rozmiarze N
* Usuwanie elementu ze struktury o rozmiarze N
* Wyszukiwanie klucza w strukturze o rozmiarze N

Gdzie N przyjmuje kolejne wartości:

* od 1 do 2000 w przypadku tablicy i listy
* od 1 do 4000000 w przypadku kopca i drzewa czerwono-czarnego

Następnie otrzymane czasy nanoszone są na wykres.

Program posiada też menu tekstowe, które pozwala w dowolnej kolejności wykonywać dowolne operacje
na wybranej przez użytkownika strukturze, pozwala wyświetlić zawartość struktury i pokazuje czas wykonania każdej operacji w mikrosekundach.
\n'''
)



document.add_header('Tablica')

document.add_text(
'''
Tablica – kontener uporządkowanych danych takiego samego typu, w którym poszczególne
elementy dostępne są za pomocą kluczy (indeksu). Indeks najczęściej przyjmuje wartości
numeryczne. Rozmiar tablicy jest albo ustalony z góry (tablice statyczne), albo może się
zmieniać w trakcie wykonywania programu (tablice dynamiczne). Tablice jednowymiarowe

mogą przechowywać inne tablice, dzięki czemu uzyskuje się tablice wielowymiarowe. W tablicach
wielowymiarowych poszczególne elementy są adresowane przez ciąg indeksów.

Praktycznie wszystkie języki programowania obsługują tablice – jedynie w niektórych
językach funkcyjnych zamiast tablic używane są listy (choć tablice zwykle też są dostępne).
W matematyce odpowiednikiem tablicy jednowymiarowej jest ciąg, a tablicy dwuwymiarowej - macierz.

'''
)

document.add_text(
'''
```
tablica.print()

[101 118 97 110 103 101 108 105 111 110]
```
'''
)

# TABLICA BENCHMARKI

img_name = 'out21.png'
#generate_plot('./a.out 21', 1, ITERATIONS, img_name, 'Czas budowania tablicy')
document.add_image(img_name)


img_name = 'out22.png'
#generate_plot('./a.out 22', 1, ITERATIONS, img_name, 'Czas usuwania pojedynczego elementu')
document.add_image(img_name)


img_name = 'out23.png'
#generate_plot('./a.out 23', 1, ITERATIONS, img_name, 'Czas wstawiania elementu w losowym punkcie tablicy')
document.add_image(img_name)


img_name = 'out24.png'
#generate_plot('./a.out 24', 1, ITERATIONS, img_name, 'Czas szukania pojedynczego elementu w tablicy')
document.add_image(img_name)



document.add_header('Lista')

document.add_text(
'''
Lista – struktura danych służąca do reprezentacji zbiorów dynamicznych, w której elementy

ułożone są w liniowym porządku. Rozróżniane są dwa podstawowe rodzaje list: lista jednokierunkowa,
w której z każdego elementu możliwe jest przejście do jego następnika oraz lista

dwukierunkowa w której z każdego elementu możliwe jest przejście do jego poprzednika i
następnika
'''
)

document.add_text(
'''
```
lista.print()

[114 105 99 97 114 100 111]
```
'''
)

# Benchmarki listy

img_name = 'out11.png'
#generate_plot('./a.out 11', 1, ITERATIONS, img_name, 'Czas budowania listy')
document.add_image(img_name)


img_name = 'out12.png'
#generate_plot('./a.out 12', 1, ITERATIONS, img_name, 'Czas usuwania pojedynczego elementu z listy')
document.add_image(img_name)


img_name = 'out13.png'
#generate_plot('./a.out 13', 1, ITERATIONS, img_name, 'Czas dodawania pojedynczego elementu na losowe miejsce listy')
document.add_image(img_name)


img_name = 'out14.png'
#generate_plot('./a.out 14', 1, ITERATIONS, img_name, 'Czas szukania pojedynczego elementu listy')
document.add_image(img_name)





document.add_header('Kopiec')

document.add_text(
'''
Kopiec (ang. heap, tłumaczone też jako stóg lub sterta) – struktura danych oparta na drzewie,
w której wartości potomków węzła są w stałej relacji z wartością rodzica (na przykład wartość
rodzica jest nie mniejsza niż wartości jego potomka).
Kopce mogą być używane do implementacji kolejek priorytetowych, ponieważ ustalenie 
odpowiedniej relacji umożliwia bezpośredni dostęp do wierzchołka o największej lub naj
mniejszej wartości z danego zbioru zapisanego w kopcu.


Drugie częste zastosowanie struktury kopca to sortowanie. Wstawianie nowych wierzchołków
do kopca porządkuje je. Następnie możemy zdejmować obiekty ze szczytu kopca, tak długo aż są w kopcu jakieś obiekty.
Otrzymamy wówczas ciąg obiektów uporządkowany według klucza. Na tej zasadzie działa procedura sortowania przez kopcowanie (ang. heapsort).
'''
)

document.add_text(
'''
```
kopiec.print()

                               96
               95                               92
       86               88               61               86
   34       78       83       50       29       54       11       52
```
'''
)

img_name = 'out31.png'
#generate_plot('./a.out 31', 1, ITERATIONS*10, img_name, 'Czas budowania kopca')
document.add_image(img_name)


img_name = 'out33.png'
#generate_plot('./a.out 33', 1, ITERATIONS*10, img_name, 'Czas dodawania pojedynczego elementu do kopca')
document.add_image(img_name)


img_name = 'out32.png'
#generate_plot('./a.out 32', 1, ITERATIONS*10, img_name, 'Czas usuwania pojedynczego elementu z kopca')
document.add_image(img_name)


img_name = 'out34.png'
#generate_plot('./a.out 34', 1, ITERATIONS*10, img_name, 'Czas szukania pojedynczego elementu w kopcu')
document.add_image(img_name)


document.add_header('Drzewo czerwono-czarne')

document.add_text(
'''
Drzewo czerwono-czarne – rodzaj samoorganizującego się binarnego drzewa poszukiwań -

struktury danych stosowanej w informatyce najczęściej do implementacji tablic asocjacyjnych.
Została ona wynaleziona przez Rudolfa Bayera w 1972 roku, który nazwał je symetrycznymi
binarnymi B-drzewami. Współczesną nazwę oraz dokładne zbadanie ich właściwości
zawdzięcza się pracy *A dichromatic framework for balanced trees* 1978 roku autorstwa 
Leo J. Guibasa oraz Roberta Sedgewicka.
Drzewa czerwono-czarne są skomplikowane w implementacji, lecz charakteryzują się **niską**
złożonością obliczeniową elementarnych operacji takich, jak wstawianie, wyszukiwanie czy
usuwanie elementów z drzewa.
'''
)


img_name = 'out41.png'
#generate_plot('./a.out 41', 1, ITERATIONS*10, img_name, 'Czas budowania drzewa')
document.add_image(img_name)


img_name = 'out42.png'
#generate_plot('./a.out 42', 1, ITERATIONS*10, img_name, 'Czas usuwania pojedynczego elementu z drzewa')
document.add_image(img_name)


img_name = 'out43.png'
#generate_plot('./a.out 43', 1, ITERATIONS*10, img_name, 'Czas dodawania pojedynczego elementu do drzewa')
document.add_image(img_name)


img_name = 'out44.png'
#generate_plot('./a.out 44', 1, ITERATIONS*10, img_name, 'Czas szukania pojedynczego elementu w drzewie')
document.add_image(img_name)

document.add_header('Wnioski')

document.add_text(
'''
Po samodzielnym zaimplementowaniu i przetestowaniu powyższych struktur danych,
najważniejsze wnioski wyraziłem w tabeli:


| Struktura | Trudność implementacji | Kiedy najlepiej użyć | Kiedy **nie używać** |
| ------ | ------ | --- | --- |
| Tablica | Dziecinnie łatwa | Dane niezmiennej długości, mało dostępnych zasobów | Częste zmiany długości tablicy |
| Lista | Łatwa | Dużo operacji dopisywania/kasowania elementów | Brak specjalnych przeciwwskazań |
| Kopiec | Średnia | Potrzeba wyszukania największej/najmniejszej wartości w zbiorze | Brak potrzeby sortowania/przechowywania w danym porządku |
| Drzewo czerwono-czarne | **Trudna** | Szybkie wyszukiwanie dowolnego klucza | Brak potrzeby szybkiego wyszukiwania, potrzeba prostej implementacji |



'''
)

document.save()
