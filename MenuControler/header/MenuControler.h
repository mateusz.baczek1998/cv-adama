#pragma once
#include<iostream>
#include"../../Array/header/Array.h"
#include"../../List/header/Lista2kier.h"
#include"../../Heap/headers/Heap.h"
#include"../../RedBlackTree/headers/RedBlackTree.h"
#include"../../Timer/header/Timer.h"


class MenuControler
{
public:

    char showMainMenu();
    char showSubMenu(std::string structName);
    void runControler(int argc, char** argv);
    
    void proceedList(Lista2Kier& list,char choice);
    void proceedArray(Array& list,char choice);
    void proceedHeap(Heap& list,char choice);
    void proceedTree(RedBlackTree& list,char choice);

    void handleArgument(int operationCode, int howManyTimes);
};