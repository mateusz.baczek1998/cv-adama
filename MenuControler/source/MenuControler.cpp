#include"../header/MenuControler.h"
#include <limits>
#include"../../Common/header/HelperFunctions.h"
#include "../../List/header/Node.h"
#include  <random>
#include  <chrono>

using namespace std;


void MenuControler::proceedList(Lista2Kier& list,char choice)
{
    Timer timer;
    switch(choice)
    {
        case  '1':
        {

            cout<<"Ile liczb załadować?";
            int ile;
            cin>>ile;
            cout<<endl;
            timer.start();
            HelperClass::addSomeValues(list,ile);
            timer.stop();
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '2':
        {
            cout<<"Podaj wartosc do usuniecia"<<endl;
            int val;
            cin>>val;
            timer.start();
            list.erase(val);
            timer.stop();
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '3':
        {
            cout<<"Podaj wartosc"<<endl;
            int val;
            cin>> val;
            cout<<"Gdzie chcesz dodac?  1 front, 2 back 3 poz"<<endl;
            char  choice;
            cin>>choice;
            if(choice=='1')
            {
                timer.start();
                list.pushFront(val);
                timer.stop();
            }
            else if(choice=='2')
            {
                timer.start();
                list.pushBack(val);
                timer.stop();
            }
            else
            {
                cout<<"Podaj pozycje"<<endl;
                int  poz;
                cin>>poz;
                timer.start();
                list.insert(val,poz);
                timer.stop();
            }
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '4':
        {
            cout<<"Podaj szukana wartosc"<<endl;
            int val;
            cin>>val;
            timer.start();
            Node* node = list.search(val);
            timer.stop();

            if(node==nullptr) cout<<"Nie znaleziono elementu"<<endl;
            else              cout<<"Znaleziono element o podanej wartosci"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '5':
        {
            HelperClass::show(list);
            cin.get();
            cin.get();
            break;
        }
        case  '6':
        {
            break;
        }
    }
}
void MenuControler::proceedArray(Array& array,char choice)
{
    Timer timer;
    switch(choice)
    {
        case  '1':
        {
            cout<<"Ile liczb załadować?";
            int ile;
            cin>>ile;
            cout<<endl;
            timer.start();
            HelperClass::addSomeValues(array,ile);
            timer.stop();
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '2':
        {
            cout<<"Podaj index elementu który chcesz usunać"<<endl;
            int index;
            cin>>index;
            timer.start();
            array.erase(index);
            timer.stop();
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '3':
        {
            cout<<"Podaj wartosc jaka chcesz dodac"<<endl;
            int val;
            cin>>val;
            cout<<"Podaj pozycje na którą wstawic"<<endl;
            int poz;
            cin>>poz;
            timer.start();
            array.insert(val,poz);
            timer.stop();
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '4':
        {
            cout<<"Jakiej wartosci szukasz"<<endl;
            int val;
            cin>>val;
            timer.start();
            int index=array.search(val);
            timer.stop();
            if(index==-1)
            {
                cout<<"Nie znaleziono wartosci"<<endl;
            }
            else
            {                     
                cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
                cout<<"Index szukanego elementu "<<index<<endl;
            }

            cin.get();
            cin.get();

            break;
        }
        case  '5':
        {
            HelperClass::show(array);
            cin.get();
            cin.get();
            break;
        }
        case  '6':
        {
            break;
        }
    }

}
void MenuControler::proceedHeap(Heap& heap,char choice)
{
  Timer timer;
    switch(choice)
    {
        case  '1':
        {

            cout<<"Ile liczb załadować?";
            int ile;
            cin>>ile;
            cout<<endl;
            timer.start();
            HelperClass::addSomeValues(heap,ile);
            timer.stop();
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '2':
        {
            cout<<"Kasowanie.."<<endl;

            //cout<<"Podaj wartosc do usuniecia"<<endl;
            //int val;
            //cin>>val;
            timer.start();
            heap.pop();//erase(val);
            timer.stop();
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '3':
        {
            cout<<"Podaj wartosc"<<endl;
            int val;
            cin>> val;
            timer.start();
            heap.insert(val);
            timer.stop();
            
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '4':
        {
            cout<<"Podaj szukana wartosc"<<endl;
            int val;
            cin>>val;
            timer.start();

            int index = heap.indexOf(val);
            //Node* node = list.search(val);
            timer.stop();

            if(index==-1) cout<<"Nie znaleziono elementu"<<endl;
            else              cout<<"Znaleziono element o podanej wartosci"<<endl;
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            
            cin.get();
            cin.get();
            break;
        }
        case  '5':
        {
            heap.print();
            cin.get();
            cin.get();
            break;
        }
        case  '6':
        {
            break;
        }
    }

}
void MenuControler::proceedTree(RedBlackTree& tree,char choice)
{
Timer timer;
    switch(choice)
    {
        case  '1':
        {
            cout<<"Ile elementow zaladowac?"<<endl;
            int ile;
            cin>>ile;
            timer.start();
            HelperClass::addSomeValues(tree,ile);
            timer.stop();
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;      
        }
        case  '2':
        {
            cout<<"Podaj element ktory chcesz usunac"<<endl;
            int val;
            cin>>val;
            timer.start();
            tree.deleteElement(val);
            timer.stop();
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '3':
        {
            cout<<"Podaj wartosc elementu jaki chcesz dodac"<<endl;
            int val;
            cin>>val;
            timer.start();
            tree.addElement(val);
            timer.stop();
            cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            cin.get();
            cin.get();
            break;
        }
        case  '4':
        {
            cout<<"Jakiej wartosci szukasz"<<endl;
            int val;
            cin>>val;
            timer.start();
            bool exist=tree.search(val);
            timer.stop();
            if(!exist)
            {
                cout<<"Nie znaleziono wartosci"<<endl;
            }
            else
            {                     
                cout<<"Czas "<<timer.getTimeInMicroSec()<<"[microsec]"<<endl;
            }

            cin.get();
            cin.get();
            break;
        }
        case  '5':
        {
            //tree.drawTree(); TODO
            break;
        }
        case  '6':
        {
            break;
        }
    }
}



char MenuControler::showMainMenu()
{
    system("clear");
    cout<<"****************************"<<endl;
    cout<<"*           MENU           *"<<endl;
    cout<<"****************************"<<endl<<endl;
    
    cout<<"Wybierz strukture na jakiej chcesz pracowac"<<endl;
    cout<<"1. Lista"<<endl;
    cout<<"2. Tablica"<<endl;
    cout<<"3. Kopiec"<<endl;
    cout<<"4. Drzewo czerwono-czarne"<<endl;
    cout<<"5. Zamknij"<<endl;

    char choice;
    while(true)
    {
       cin>>choice;
        if(cin.fail() || choice<'1' || choice >'5')
        {
            cout<<"Podales zly znak. Spróbuj ponownie:"<<endl;
            cin.clear();
            cin.ignore(std::numeric_limits<streamsize>::max(),'\n');
            continue;
        }
        break;
    }

    return choice;
}

char MenuControler::showSubMenu(std::string structName)
{
    system("clear");

    cout<<structName<<endl;
    cout<<"*****************"<<endl;

    cout<<"1. Zbuduj z pliku"<<endl;
    cout<<"2. Usun"<<endl;
    cout<<"3. Dodaj"<<endl;
    cout<<"4. Znajdź"<<endl;
    cout<<"5. Wyświetl"<<endl;
    cout<<"6. Wróc"<<endl;

    char choice;
    while(true)
    {
       cin>>choice;
        if(cin.fail() || choice<'1' || choice >'6')
        {
            cout<<"Podales zly znak. Spróbuj ponownie: "<<endl;
            cin.clear();
            cin.ignore(std::numeric_limits<streamsize>::max(),'\n');
            continue;
        }          
        break;
    }
    return choice;
}

void MenuControler::runControler(int argc, char* argv[])
{
    char choice1, choice2;


    while(true)
    {
        choice1=choice2=0;

        if(argc==1)                             // jest  1 bo zawsze podawany jest kaalog domowy
        {
        choice1 = showMainMenu();
        }
        else if(argc==3)
        {
            MenuControler::handleArgument(atoi(argv[1]), atoi(argv[2]));
            exit(0);
        } else {
            cout << "Wrong parameters!" << endl;
            cout << "a.out [operation_code how_many_tries]" << endl;
            exit(0);
        }

        if(choice1 == '5') break;

        switch(choice1)
        {
            case'1':
            {
                Lista2Kier lista;

                while(choice2!='6')
                {
                choice2 =this->showSubMenu("Lista");
                proceedList(lista,choice2);
                }
                break;
            }
            case'2':
            {
                Array array;

                while(choice2!='6')
                {
                    choice2=this->showSubMenu("Tablica");
                    proceedArray(array,choice2);
                }
                break;
            }
            case'3':
            {
                Heap heap(1000);

                while(choice2!='6')
                {
                choice2 =this->showSubMenu("Kopiec");
                proceedHeap(heap,choice2);
                }

                break;
            }
            case'4':
            {
                RedBlackTree tree;

                while(choice2!='6')
                {
                    choice2=this->showSubMenu("Drzewo czerwono-czarne");
                    proceedTree(tree,choice2);
                }
                break;
            }

        }
    }
}

void MenuControler::handleArgument(int operationCode, int howManyTimes) {

    //cout << "handling " << operationCode << endl;

    Timer timer;
    
    switch(operationCode) {


        //////////////
        //          //
        //  LISTA   //
        //          //
        //////////////

        case 11: //Lista laduje dane
        {   
            Lista2Kier lista;
            timer.start();
            HelperClass::addSomeValues(lista,howManyTimes);
            timer.stop();
            break;
        }
        case 12: // Lista usuwa podana wartosc
        {
            Lista2Kier lista;
            HelperClass::addSomeValues(lista,howManyTimes);

            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int valueToRemove = distr(engine);

            timer.start();
            lista.erase(valueToRemove);
            timer.stop();
            break;
        }
        case 13: //Lista dodaje element (nalezy jeszcze podac wartosc i wybrać opcje(1-na początek,2- na koniez,3- w wybrane miejce które należy potem podać))
        {
            Lista2Kier lista;
            HelperClass::addSomeValues(lista, howManyTimes);

            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int valueToInsert = distr(engine);
            int poz = distr(engine);

            timer.start();
            lista.insert(valueToInsert,poz);
            timer.stop();
            break;
        }
        case 14://Lita znaduje element o podanej  wartosci
        {
            Lista2Kier lista;
            HelperClass::addSomeValues(lista, howManyTimes);

            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int valueToSearch = distr(engine);

            timer.start();
            lista.search(valueToSearch);
            timer.stop();

            break;
        }
        case 15://Lista wyswietla elementy  listy
        {
            Lista2Kier lista;

            HelperClass::addSomeValues(lista,howManyTimes);

            timer.start();
            HelperClass::show(lista);
            timer.stop();
            break;
        }

        //////////////
        //          //
        // TABLICA  //
        //          //
        //////////////

        case 21: // Tablica laduje dane
        {
            Array tablica;
            timer.start();
            HelperClass::addSomeValues(tablica,howManyTimes);
            timer.stop();
            break;
        }
        case 22: //Tablica usuwa element o podanym indeksie
        {
            Array tablica;
            HelperClass::addSomeValues(tablica,howManyTimes);

            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int index = distr(engine);

            timer.start();
            tablica.erase(index);
            timer.stop();
            break;
        }
        case 23: //Tablica dodaje element o podanej wartosci
        {
            Array tablica;
            HelperClass::addSomeValues(tablica,howManyTimes);

            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int val = distr(engine);

            timer.start();
            tablica.insert(val,val);
            timer.stop();
            break;
        }
        case 24: //Tablica znajduje element o podanej wartości
        {
            Array tablica;
            HelperClass::addSomeValues(tablica,howManyTimes);

            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int val = distr(engine);

            timer.start();
            tablica.search(val);
            timer.stop();
            break;
        }
        case 25:// Tablica wyświetla elementy
        {
            Array tablica;
            HelperClass::addSomeValues(tablica,howManyTimes);

            timer.start();
            HelperClass::show(tablica);
            timer.stop();
            break;
        }

        //////////////
        //          //
        //  KOPIEC  //
        //          //
        //////////////

        case 31: // Dodawanie n elementów do kopca
        {
            Heap heap(howManyTimes);

            timer.start();
            HelperClass::addSomeValues(heap, howManyTimes);
            timer.stop();
        break;
        }
        case 32: // Extract-max z kopca o n elementów
        {
            Heap heap(howManyTimes);
            HelperClass::addSomeValues(heap, howManyTimes);

            timer.start();
            heap.pop();
            timer.stop();

            break;
        }
        case 33: // Dodanie jednego elementu do kopca o n elementach
        {
            Heap heap(howManyTimes+10);
            HelperClass::addSomeValues(heap, howManyTimes);

            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int valueToInsert = distr(engine);

            timer.start();
            heap.insert(valueToInsert);
            timer.stop();

            break;
        }
        case 34:
        {
            Heap heap(howManyTimes);
            HelperClass::addSomeValues(heap, howManyTimes);

            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int valueToFind = distr(engine);

            timer.start();
            heap.indexOf(valueToFind); // TODO: MAKE RANDOM
            timer.stop();

            break;
        }
        case 41:
        {   
            RedBlackTree tree;

            timer.start();
            HelperClass::addSomeValues(tree, howManyTimes);
            timer.stop();
            break;
        }
        case 42:
        {
            RedBlackTree tree;
            HelperClass::addSomeValues(tree, howManyTimes);
            
            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int valueToRemove = distr(engine);

            timer.start();
            tree.deleteElement(valueToRemove);
            timer.stop();

            break;
        }
        case 43:
        {
            RedBlackTree tree;
            HelperClass::addSomeValues(tree, howManyTimes);
            
            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int valueToInsert = distr(engine);

            timer.start();
            tree.addElement(valueToInsert);
            timer.stop();
            
            break;
        }
        case 44:
        {
            RedBlackTree tree;
            HelperClass::addSomeValues(tree, howManyTimes);
            
            size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
	        std::default_random_engine engine(seed);
	        std::uniform_int_distribution<int>  distr(0,1000);

            int valueToFind = distr(engine);

            timer.start();
            tree.search(valueToFind);
            timer.stop();
            
            break;
        }

    }

    cout << timer.getTimeInMicroSec() << endl;
}