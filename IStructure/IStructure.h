# pragma once 
#include"../List/header/Node.h"

template<typename TVal>  // Dla listy TVal=int 
class IStructure
{
public:

    virtual bool isEmpty() = 0;						//sprawdza czy lista jest pusta
	virtual bool pushFront(TVal value) = 0;			//dodaje element inicjalizowany wartoscią z argumentu
	virtual bool pushBack(TVal value) = 0;			//dodaje element inicjalizowany wartościa z argummentu  na koniec listy
	virtual bool insert(TVal value,int position) = 0;//dodaje element inicjalizowany wartoscią z argumentu, na losowe miejsce na liscie
	virtual bool erase(TVal value) = 0;				//usuwa element o podanej wartosci 
    
};